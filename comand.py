#from distutils.cmd import Command
from multiprocessing import Event
import threading
import serial
import time
from threading import Thread
import sys

ser = serial.Serial("COM8", 115200, timeout=None)   #Configuration of connection to the device via COM port

print("\r\n**************"" CY8CPROTO-062-4343W PSoC 6 ""*************** \r\n" )

def menu():
    print("*********************** HELP MENU *********************** \r\n\n"
          "********** Please, enter one of these command: ********** \r\n"
          "_________________________________________________________\r\n "
            "- *Stop* - Stop temperature measurement;\r\n "
            "- *Start* - Start measurement with default frequency\r\n"
            "                 (1 second) or with a previous frequency;\r\n "
            "- *Change, X* - Change measurement period (in seconds), \r\n"
            "                  X - number of seconds;\r\n "
            "- *Exit* - exit the programm.\r\n"
           "_________________________________________________________\r\n ")
    
def record_to_file():   #Function for reading data and writing time and temperature values to a file
    while(1):
        data = ser.readline()
        f = open('Temperature.txt','a') 
        getTime = str(time.strftime('%d-%B-%y %H:%M:%S '))
        data = str(data, 'UTF-8')
        infoLine = (getTime, data)
        f.writelines(infoLine)
        f.close()
  
#exeptions
def is_number(str): #Check that the value entered is a number
    try:
        float(str)
        return True
    except ValueError:
        return False
   
def is_positive(str):   #Check that the value entered is positive
    try:
        val = int(str)
        if val >= 0:
            return True
    except ValueError:
        return False 

record_thread = Thread(target=record_to_file)   #Create the thread responsible for writing data to a file
event = threading.Event()
command_counter = 1 #Variable to check the number of start commands, because the thread can be run only once, 
                    #if this variable = 1, start the thread, if > 1, resume the thread
   
menu()
while (1):
    print ("\r\nCommand:\r\n")
    user_command = input()


    if len(user_command) > 20:
        print("\n\rThe size of the command is incorrect\n\r")
        menu()

    else:    
        arr = user_command.split(',')
        arr_len = len(arr)
        if arr[0] == "Stop":
            if arr_len == 1:
                ser.write((user_command + '\r').encode())
                print("\r\nMeasurement stopped\r\n")
                event.clear()

        elif arr[0] == "Start":
            if arr_len == 1:
                ser.write((user_command + '\r').encode()) 
                print("\n\rMeasurement started\n\r")
                if(command_counter == 1):
                    record_thread.start()
                    command_counter+=1
                elif(command_counter > 1):
                    event.set()
                    command_counter+=1
                
        elif arr[0] == "Change":
            if arr_len != 2:
               print("\n\rNo parameter *period*!\n\r") 
               menu()
            else:
                if is_number(arr[1]) == True and is_positive(arr[1]) == True:
                    ser.write((user_command+'\r').encode())
                    print("\n\rPeriod of measurment is changed\n\r")
                    event.set()
                else:
                    print("\n\rValue of period is not correct!\n\r")
                    
        elif arr[0] == "Exit":
            if arr_len ==1:
                exit() 

        else:
            print("\n\rIncorrect command, please try again\n\r")
            menu()
