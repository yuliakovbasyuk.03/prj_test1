#include "cy_pdl.h"
#include "cyhal.h"
#include "cybsp.h"
#include "cy_retarget_io.h"
#include "math.h"
#include <stdlib.h>
#include <stdbool.h>
#include <cy_sysanalog.h>

/*******************************************************************************
 * Macros
 *******************************************************************************/

#define COMMAND_SIZE                    (20u)

#define ASCII_RETURN_CARRIAGE           (0x0D)

#define B_const 						(int)(3380) /*B-Constant(25/50℃), Reference Value
													* from the documentation for NCP18XH103F03RB.
													* B (or Beta) constant - value that represents the relationship between
													* the resistance and temperature over a specified temperature range.*/

#define VREFplus 						(int)(3555)	//Voltage value VREF(3300) + 2^n - 1

#define UART_TIMEOUT_MS                 (1u)

#define TIMER_CLOCK_HZ         		    (10000)	//Timer clock value in Hz

/* State machine */
typedef enum {
	MESSAGE_ENTER_NEW, 		//Start to enter new command
	MESSAGE_Start,			//Start measurement(number)
	MESSAGE_Stop,			//Stop measurement(number)
	MESSAGE_Change,			//Change period  of measurement(number in seconds)
	MESSAGE_NOT_READY,		//Enter new command
} message_status_t;

/*******************************************************************************
 * Global Variables
 *******************************************************************************/

CY_ALIGN(4) uint8_t message[COMMAND_SIZE];

char separator[1] = ",";

/* Timer object*/
cyhal_timer_t timer;
int TIMER_PERIOD;
int new_period;

bool timer_interrupt_flag = false;
;

float temperature;
float resistance;
float voltage;

/*******************************************************************************
 * Function Prototypes
 *******************************************************************************/

cy_rslt_t Enter_MSG(uint8_t message[], uint8_t msg_size);//Get command from user

float get_temperature(float voltage, float resistance);	//Calculate the temperature value from the equation

void timer_init(void);	//Initialize and set timer settings

static void isr_timer(void *callback_arg, cyhal_timer_event_t event);//Callback timer interrupt function

/*******************************************************************************
 * Function Name: handle_error*
 *******************************************************************************/
void handle_error(void) {
	/* Disable all interrupts. */
	__disable_irq();

	CY_ASSERT(0);
}

/*******************************************************************************
 * Function Name: main
 *******************************************************************************/
int main(void) {

	cy_rslt_t result = CY_RSLT_SUCCESS;
	cy_rslt_t uart_result = CY_RSLT_SUCCESS;

	message_status_t msg_status = MESSAGE_ENTER_NEW;

	uint8_t msg_size = 0;

	char *istr;
	bool uart_status = false;

	/* Initialize the device and board peripherals */
	result = cybsp_init();
	if (result != CY_RSLT_SUCCESS) {
		handle_error();
	}

	/* Initialize retarget-io to use the debug UART port */
	result = cy_retarget_io_init(CYBSP_DEBUG_UART_TX, CYBSP_DEBUG_UART_RX,
			CY_RETARGET_IO_BAUDRATE);
	if (result != CY_RSLT_SUCCESS) {
		handle_error();
	}

	uint16_t resADC[0]; //ADC channel out(P10_1)

	/* AREF */
	cy_en_sysanalog_status_t status_aref;

	status_aref = Cy_SysAnalog_Init(&Cy_SysAnalog_Fast_Local);

	/* Turn on the hardware block. */
	Cy_SysAnalog_Enable();

	if (status_aref != CY_RSLT_SUCCESS) {
		CY_ASSERT(0);
	}

	/*enable SAR ADC */

	result = Cy_SAR_Init(SAR, &pass_0_sar_0_config);
	if (result != CY_RSLT_SUCCESS) {
		CY_ASSERT(0);
	}
	Cy_SAR_Enable(SAR);

	/*The USER LED was added on its own initiative for a clearer demonstration of the program's work
	 * as a signal to the user about the status of programs.
	 * The light is on only when the measurement and recording in the file is taking place,
	 * if the measurement and recording in the file is stopped - the light does not light up.*/
	result = cyhal_gpio_init(CYBSP_USER_LED, CYHAL_GPIO_DIR_OUTPUT,
			CYHAL_GPIO_DRIVE_STRONG, CYBSP_LED_STATE_OFF);

	/* GPIO init failed. Stop program execution */
	if (result != CY_RSLT_SUCCESS) {
		CY_ASSERT(0);
	}

	__enable_irq();

	timer_init();

	for (;;) {

		switch (msg_status) {

		case MESSAGE_ENTER_NEW:
			msg_size = 0;
			uart_result = Enter_MSG(message, msg_size);
			msg_status = MESSAGE_NOT_READY;
			break;

		case MESSAGE_NOT_READY:
			uart_status = cyhal_uart_is_rx_active(&cy_retarget_io_uart_obj);

			if ((!uart_status) && (uart_result == CY_RSLT_SUCCESS)) {
				if (message[msg_size] == ASCII_RETURN_CARRIAGE) {
					message[msg_size] = '\0';

					istr = strtok((char*) message, separator);
					while (istr != NULL) {
						if ((!strcmp(istr, "Stop"))) {
							cyhal_timer_stop(&timer);
							msg_status = MESSAGE_Stop;
							break;
						} else if ((!strcmp((char*) message, "Start"))) {
							cyhal_timer_start(&timer);
							msg_status = MESSAGE_Start;
							break;
						} else if (!strcmp(istr, "Change")) {
							istr = strtok(NULL, separator);
							msg_status = MESSAGE_Change;
							break;
						}

					}
				} else {
					cyhal_uart_putc(&cy_retarget_io_uart_obj,
							message[msg_size]);
					msg_size++;
				}
			}
			uart_result = cyhal_uart_getc(&cy_retarget_io_uart_obj,
					&message[msg_size],
					UART_TIMEOUT_MS);
			break;

		case MESSAGE_Stop:
			cyhal_gpio_write(CYBSP_USER_LED, CYBSP_LED_STATE_OFF);
			msg_status = MESSAGE_ENTER_NEW;

			break;

		case MESSAGE_Start:
			msg_status = MESSAGE_ENTER_NEW;

			break;

		case MESSAGE_Change:
			new_period = (int) (atof(istr) * 10000);
			TIMER_PERIOD = (int) (new_period - 1);

			cyhal_timer_cfg_t timer_cfg = { .compare_value = 0, /* Timer compare value, not used */
			.period = TIMER_PERIOD, /* Defines the timer period */
			.direction = CYHAL_TIMER_DIR_UP, /* Timer counts up */
			.is_compare = false, /* Don't use compare mode */
			.is_continuous = true, /* Run timer indefinitely */
			.value = 0 /* Initial value of counter */
			};
			cyhal_timer_configure(&timer, &timer_cfg);
			msg_status = MESSAGE_ENTER_NEW;

			break;

		default:
			break;
		}

		if (timer_interrupt_flag) {
			// Clear the flag
			timer_interrupt_flag = false;
			//Get voltage from ADC out
			Cy_SAR_StartConvert(SAR, CY_SAR_START_CONVERT_SINGLE_SHOT);
			Cy_SAR_IsEndConversion(SAR, CY_SAR_WAIT_FOR_RESULT);
			resADC[0] = Cy_SAR_GetResult16(SAR, 0);
			voltage = Cy_SAR_CountsTo_mVolts(SAR, 0, resADC[0]);
			cyhal_gpio_write(CYBSP_USER_LED, CYBSP_LED_STATE_ON);
			printf("  Temperature = %.1f Celsius \r\n",
					get_temperature(voltage, resistance));
		}
	}
}

cy_rslt_t Enter_MSG(uint8_t message[], uint8_t msg_size) {
	memset(message, 0, COMMAND_SIZE);
	return cyhal_uart_getc(&cy_retarget_io_uart_obj, &message[msg_size],
	UART_TIMEOUT_MS);
}

float get_temperature(float voltage, float resistance) {
	resistance = (float) (VREFplus - voltage) * 10000 / voltage;
	temperature = 1 / (logf(resistance / 10000) / B_const + 1 / 298.15)
			- 273.15;
	return temperature;
}

void timer_init(void) {
	cy_rslt_t result;

	cyhal_timer_cfg_t timer_cfg = { .compare_value = 0, /* Timer compare value, not used */
	.period = 9999, /* Defines the timer period */
	.direction = CYHAL_TIMER_DIR_UP, /* Timer counts up */
	.is_compare = false, /* Don't use compare mode */
	.is_continuous = true, /* Run timer indefinitely */
	.value = 0 /* Initial value of counter */
	};

	/* Initialize the timer object. Does not use input pin ('pin' is NC) and
	 * does not use a pre-configured clock source ('clk' is NULL). */
	result = cyhal_timer_init(&timer, NC, NULL);

	/* timer init failed. Stop program execution */
	if (result != CY_RSLT_SUCCESS) {
		CY_ASSERT(0);
	}

	/* Configure timer period and operation mode such as count direction,
	 duration */
	cyhal_timer_configure(&timer, &timer_cfg);

	/* Set the frequency of timer's clock source */
	cyhal_timer_set_frequency(&timer, TIMER_CLOCK_HZ);

	/* Assign the ISR to execute on timer interrupt */
	cyhal_timer_register_callback(&timer, isr_timer, NULL);

	/* Set the event on which timer interrupt occurs and enable it */
	cyhal_timer_enable_event(&timer, CYHAL_TIMER_IRQ_TERMINAL_COUNT, 3, true);

}

static void isr_timer(void *callback_arg, cyhal_timer_event_t event) {
	(void) callback_arg;
	(void) event;

	/* Set the interrupt flag and process it from the main while(1) loop */
	timer_interrupt_flag = true;
}

/* [] END OF FILE */
